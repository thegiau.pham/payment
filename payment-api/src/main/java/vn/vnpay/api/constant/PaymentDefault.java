package vn.vnpay.api.constant;

public class PaymentDefault {
    public static final String BANK_CODE = "970445";
    public static final Integer MESSAGE_TYPE = 1;
    public static final String ADD_VALUE = "{\"payMethod\":\"01\",\"payMethodMMS\":1}";
    public static final String DATE_TIME_FORMAT = "yyyyMMddHHmmss";
    public static final String SESSION_TOKEN = "session";
}
