package vn.vnpay.api.validate;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.vnpay.api.config.BankConfig;
import vn.vnpay.api.constant.PaymentDefault;
import vn.vnpay.api.dto.PaymentDTO;
import vn.vnpay.api.enums.MessageType;
import vn.vnpay.api.enums.PaymentTime;
import vn.vnpay.api.exception.CustomException;
import vn.vnpay.api.model.BankEntity;
import vn.vnpay.api.service.impl.RedisServiceImpl;
import vn.vnpay.api.util.DateTimeUtil;
import vn.vnpay.api.util.HashUtil;

import java.time.LocalDateTime;

@Component
public class PaymentValidator extends Validator{
    @Autowired
    private BankConfig bankConfig;

    @Autowired
    private RedisServiceImpl redisService;

    @Override
    public void validateToken(PaymentDTO model) throws CustomException {
        boolean isExist =  redisService.exist(model.getTokenKey());
        if (isExist) {
            throw new CustomException(
                    MessageType.TOKEN_EXIST.getCode(),
                    MessageType.TOKEN_EXIST.getMessage()
            );
        }
    }

    @Override
    public void validateAmount(PaymentDTO model) throws CustomException {
        int compareResult = model.getRealAmount().compareTo(model.getDebitAmount());
        if (compareResult > 0) {
            throw new CustomException(
                    MessageType.AMOUNT_NOT_VALID.getCode(),
                    MessageType.AMOUNT_NOT_VALID.getMessage()
            );
        }
        if (compareResult < 0) {
            if (Strings.isBlank(model.getPromotionCode())) {
                throw new CustomException(
                        MessageType.VOUCHER_NOT_NULL.getCode(),
                        MessageType.VOUCHER_NOT_NULL.getMessage()
                );
            }
        }
    }

    @Override
    public void validateDate(PaymentDTO model) throws CustomException {
        LocalDateTime currentDate = DateTimeUtil.getInstance().getCurrentLocalDateTime();
        LocalDateTime paymentDate = DateTimeUtil.getInstance().getLocalDateTimeWithFormat(model.getPayDate(), PaymentDefault.DATE_TIME_FORMAT);
        Long durationMinutes = DateTimeUtil.getInstance().getDurationLocalDateTime(currentDate, paymentDate);

        if (durationMinutes > PaymentTime.LIMIT.getValue()) {
            throw new CustomException(
                    MessageType.PAYMENT_TIME_LIMIT.getCode(),
                    MessageType.PAYMENT_TIME_LIMIT.getMessage()
            );
        }
    }

    @Override
    public void validateChecksum(PaymentDTO model) throws CustomException {
        BankEntity bankEntity = bankConfig.getBankEntity(model.getBankCode());
        String checkSum = HashUtil.getInstance().getSHA256Hash(generateCheckSum(model, bankEntity));
        boolean isMatch =  model.getCheckSum().equals(checkSum);
        if (!isMatch)
            throw new CustomException (
                    MessageType.CHECK_SUM_NOT_VALID.getCode(),
                    MessageType.CHECK_SUM_NOT_VALID.getMessage()
            );
    }

    @Override
    public void validateBankCode(PaymentDTO model) throws CustomException {
        boolean isBankExist = bankConfig.getBanks()
                .stream()
                .anyMatch(bankEntity -> bankEntity.getBankCode().equals(model.getBankCode()));
        if(!isBankExist) {
            throw new CustomException(
                    MessageType.BANK_CODE_NOT_EXITS.getCode(),
                    MessageType.BANK_CODE_NOT_EXITS.getMessage()
            );
        }
    }

    private String generateCheckSum(PaymentDTO model, BankEntity bankEntity) {
        return new StringBuilder()
                .append(model.getMobile())
                .append(model.getBankCode())
                .append(model.getAccountNo())
//                .append(model.getPayDate())
                .append(model.getDebitAmount())
                .append(model.getRespCode())
                .append(model.getTraceTransfer())
                .append(model.getMessageType())
                .append(bankEntity.getPrivateKey())
                .toString();
    }
}
