package vn.vnpay.api.validate;

import vn.vnpay.api.dto.PaymentDTO;
import vn.vnpay.api.exception.CustomException;

public abstract class Validator {
    abstract void validateToken(PaymentDTO model) throws CustomException;
    abstract void validateAmount(PaymentDTO model) throws CustomException;
    abstract void validateDate(PaymentDTO model) throws CustomException;
    abstract void validateChecksum(PaymentDTO model) throws CustomException;
    abstract void validateBankCode(PaymentDTO model) throws CustomException;
}
