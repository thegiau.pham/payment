package vn.vnpay.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.vnpay.api.config.RabbitMQConfig;
import vn.vnpay.api.constant.PaymentDefault;
import vn.vnpay.api.dto.PaymentDTO;
import vn.vnpay.api.exception.CustomException;
import vn.vnpay.api.service.impl.PaymentServiceImpl;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/v1")
public class PaymentController extends BaseController {

    private final PaymentServiceImpl paymentService;

    private static final Logger LOGGER = LogManager.getLogger(PaymentController.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    public PaymentController(PaymentServiceImpl paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping(path = "/send-data-rabbitmq")
    public ResponseEntity<?> sendDataToRabbit(@Valid @RequestBody PaymentDTO model) throws CustomException {
        String token = paymentService.generateValidToken(model.getTokenKey());
        model.setTokenKey(token);
        ThreadContext.put(PaymentDefault.SESSION_TOKEN, token);
        try {
            return response(paymentService.sendDataToRabbit(model));
        } catch (Exception ex) {
            LOGGER.error("Exception send data to rabbit: ", ex);
            return response(error(ex));
        } finally {
            ThreadContext.clearAll();
        }
    }
}
