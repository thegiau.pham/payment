package vn.vnpay.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import vn.vnpay.api.common.IErrorResult;
import vn.vnpay.api.model.ResultEntity;

@Component
@CrossOrigin("*")
public class BaseController implements IErrorResult {
    protected ResponseEntity<?> response(ResultEntity model) {
        return new ResponseEntity<>(model, HttpStatus.OK);
    }
}
