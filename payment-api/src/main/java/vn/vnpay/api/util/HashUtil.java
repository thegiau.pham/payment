package vn.vnpay.api.util;

import vn.vnpay.api.enums.MessageType;
import vn.vnpay.api.exception.CustomException;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.Random;

public class HashUtil {
    private static HashUtil instance;

    private final String ENCODING = "UTF-8";
    private final String SHA_256 = "SHA-256";
    private final String MD5 = "MD5";

    private HashUtil() {}

    public static HashUtil getInstance() {
        if (instance == null) {
            synchronized (HashUtil.class) {
                if (null == instance) {
                    instance = new HashUtil();
                }
            }
        }
        return instance;
    }

    public String generateToken(int length) {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(leftLimit)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }

    public String getSHA256Hash(String data) throws CustomException {
        try {
            MessageDigest digest = MessageDigest.getInstance(this.SHA_256);
            byte[] hash = digest.digest(data.getBytes(this.ENCODING));
            return bytesToHex(hash);
        } catch (Exception ex) {
            throw new CustomException
                    (MessageType.SERVER_ERROR.getCode(), ex);
        }
    }

    public String getMD5Hash(String data) throws CustomException {
        try {
            MessageDigest digest = MessageDigest.getInstance(this.MD5);
            byte[] hash = digest.digest(data.getBytes(this.ENCODING));
            return bytesToHex(hash).toUpperCase();
        } catch (Exception ex) {
            throw new CustomException
                    (MessageType.SERVER_ERROR.getCode(), ex);
        }
    }

    public String bytesToHex(byte[] hash) {
        return DatatypeConverter.printHexBinary(hash);
    }
}
