package vn.vnpay.api.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.vnpay.api.enums.MessageType;
import vn.vnpay.api.exception.CustomException;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateTimeUtil {

    private static DateTimeUtil instance;

    private static final Logger LOGGER = LogManager.getLogger(DateTimeUtil.class);

    private DateTimeUtil () {};

    public static DateTimeUtil getInstance() {
        if (instance == null) {
            synchronized (DateTimeUtil.class) {
                if (null == instance) {
                    instance = new DateTimeUtil();
                }
            }
        }
        return instance;
    }

    // Get current local date time
    public LocalDateTime getCurrentLocalDateTime () {
        return LocalDateTime.now().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    // Get duration local date time
    public Long getDurationLocalDateTime(LocalDateTime date1, LocalDateTime date2) {
        Duration duration = Duration.between(date1, date2);
        return Math.abs(duration.toMinutes());
    }

    // Get local date time with pattern
    public LocalDateTime getLocalDateTimeWithFormat(String time, String pattern) throws CustomException {
        try {
            DateTimeFormatter f = DateTimeFormatter.ofPattern(pattern);
            return LocalDateTime.parse(time, f);
        } catch (DateTimeParseException e) {
            throw new CustomException(
                    MessageType.DATE_TIME_FORMAT_NOT_VALID.getCode(),
                    MessageType.DATE_TIME_FORMAT_NOT_VALID.getMessage()
            );
        }
    }
}
