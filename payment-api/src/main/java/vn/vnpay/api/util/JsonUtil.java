package vn.vnpay.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.vnpay.api.enums.MessageType;
import vn.vnpay.api.exception.CustomException;
import vn.vnpay.api.model.ResultEntity;

public class JsonUtil{

    private static JsonUtil instance;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private JsonUtil () {}

    private static final Logger LOGGER = LogManager.getLogger(JsonUtil.class);

    public static JsonUtil getInstance() {
        if(instance == null) {
            synchronized (JsonUtil.class) {
                if(null == instance) {
                    instance = new JsonUtil();
                }
            }
        }
        return instance;
    }

    public ResultEntity toEntity(String json) throws CustomException {
        try {
            return this.objectMapper.readValue(json, ResultEntity.class);
        } catch (Exception ex) {
            LOGGER.error("Exception convert json to object: ", ex);
            throw new CustomException(MessageType.SERVER_ERROR.getCode(), ex);
        }
    }

    public String convertObjectToJSON(Object obj) throws CustomException {
        try {
            return this.objectMapper.writeValueAsString(obj);
        } catch (Exception ex) {
            LOGGER.error("Exception convert object to json: ", ex);
            throw new CustomException(MessageType.SERVER_ERROR.getCode(), ex);
        }
    }
}
