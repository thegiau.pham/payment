package vn.vnpay.api.common;

import vn.vnpay.api.enums.MessageType;
import vn.vnpay.api.exception.CustomException;
import vn.vnpay.api.model.ResultEntity;

public interface IErrorResult {
    default ResultEntity error(Exception ex) {
        ResultEntity resultEntity = new ResultEntity();
        resultEntity.setMessage(ex.getMessage());
        if (ex instanceof CustomException) {
            CustomException customException = (CustomException) ex;
            resultEntity.setCode(customException.getCode());
            resultEntity.setData(customException.getData());
        }
        else {
            resultEntity.setCode(MessageType.SERVER_ERROR.getCode());
            resultEntity.setMessage(MessageType.SERVER_ERROR.getMessage());
        }
        return resultEntity;
    }
}
