package vn.vnpay.api.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import vn.vnpay.api.model.BankEntity;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "banks")
public class BankConfig {
    List<BankEntity> banks = new ArrayList<>();

    public BankEntity getBankEntity(String bankCode) {
        return this.getBanks()
                .stream()
                .filter(bankEntity1 -> bankEntity1.getBankCode().equals(bankCode))
                .findAny()
                .orElse(null);
    }
}
