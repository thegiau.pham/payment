package vn.vnpay.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultEntity {
    private String code;
    private String message;
    private String responseId;
    private String checkSum;
    private Object data;
}
