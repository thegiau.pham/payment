package vn.vnpay.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.vnpay.api.constant.PaymentDefault;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentDTO implements Serializable {

    private String mobile;
    private String additionalData;
    private String respDesc;
    private String traceTransfer;
    private String promotionCode;
    private String bankCode = PaymentDefault.BANK_CODE;
    private Integer messageType = PaymentDefault.MESSAGE_TYPE;
    private String addValue = PaymentDefault.ADD_VALUE;

//    @NotNull(message = "Token must be not null or empty!")
//    @NotBlank(message = "Token must be not null or empty!")
    private String tokenKey;

//    @NotNull(message = "API ID must be not null or empty!")
    @NotBlank(message = "API ID must be not null or empty!")
    private String apiID;

//    @NotNull(message = "Account number must be not null or empty!")
    @NotBlank(message = "Account number must be not null or empty!")
    private String accountNo;

//    @NotNull(message = "Payment date must be not null or empty!")
    @NotBlank(message = "Payment date must be not null or empty!")
    private String payDate;

    //    @NotNull(message = "Debit amount must be not null or empty!")
    @DecimalMin(value = "0.0", inclusive = false, message = "Debit amount must be greater than or equal to 0")
    private BigDecimal debitAmount;

//    @NotNull(message = "Resp code must be not null or empty!")
    @NotBlank(message = "Resp code must be not null or empty!")
    private String respCode;

//    @NotNull(message = "Checksum must be not null or empty!")
    @NotBlank(message = "Checksum must be not null or empty!")
    private String checkSum;

//    @NotNull(message = "Order code must be not null or empty!")
    @NotBlank(message = "Order code must be not null or empty!")
    private String orderCode;

    //    @NotNull(message = "Real amount must be not null or empty!")
    @DecimalMin(value = "0.0", inclusive = false, message = "Real amount must be greater than or equal to 0")
    private BigDecimal realAmount;
}
