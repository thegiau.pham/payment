package vn.vnpay.api.enums;

interface IOption {
    String getCode();
    String getMessage();
}

public enum MessageType implements IOption {
    SUCCESS("00", "Success"),
    SERVER_ERROR("01", "Internal server error"),
    VALIDATION_ERROR("02", "Validation error"),
    BANK_CODE_NOT_EXITS("03", "Bank code is not exist"),
    CHECK_SUM_NOT_VALID("04", "Check sum is not valid"),
    DATE_TIME_FORMAT_NOT_VALID("05", "Date time format is not valid"),
    AMOUNT_NOT_VALID("06", "Debit amount must be greater than or equal to real amount"),
    VOUCHER_NOT_NULL("07", "Voucher code must be not null"),
    PAYMENT_TIME_LIMIT("08", "Payment request time out"),
    TOKEN_EXIST("09", "Token is exist")
    ;

    private final String value;
    private final String message;

    MessageType(String value, String message) {
        this.value = value;
        this.message = message;
    }

    @Override
    public String getCode() {
        return value;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
