package vn.vnpay.api.enums;

public enum PaymentTime {
    LIMIT(5L)
    ;
    private final Long value;
    PaymentTime(Long value) {
        this.value = value;
    }

    public Long getValue () {
        return this.value;
    }
}
