package vn.vnpay.api.service;

import vn.vnpay.api.exception.CustomException;

public interface IRabbitMQSender <T> {
    String send(T object) throws CustomException;
}
