package vn.vnpay.api.service;

import vn.vnpay.api.dto.PaymentDTO;
import vn.vnpay.api.exception.CustomException;
import vn.vnpay.api.model.ResultEntity;

public interface IPaymentService {
    ResultEntity sendDataToRabbit(PaymentDTO model) throws CustomException;
}
