package vn.vnpay.api.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import vn.vnpay.api.exception.CustomException;
import vn.vnpay.api.service.IRedisService;

@Service
public class RedisServiceImpl implements IRedisService {

    private static final Logger LOGGER = LogManager.getLogger(RedisServiceImpl.class);

    private final JedisPool jedisPool;

    @Autowired
    public RedisServiceImpl(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    @Override
    public Boolean exist(String token) throws CustomException {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.exists(token);
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
    }

    @Override
    public Long setDataWithExpiredTime(String token, Long unixTime) throws CustomException {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.setnx(token, token);
            return jedis.expireAt(token, unixTime);
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
    }
}
