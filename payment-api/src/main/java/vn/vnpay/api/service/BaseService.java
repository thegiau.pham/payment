package vn.vnpay.api.service;

import org.springframework.stereotype.Service;
import vn.vnpay.api.exception.CustomException;

@Service
public class BaseService {
    protected CustomException getCustomException (String code, String message, Object... object) {
        return new CustomException(code, String.format(message, object));
    }
}
