package vn.vnpay.api.service;

import vn.vnpay.api.exception.CustomException;

public interface IRedisService {

    // Check token exist in redis
    Boolean exist(String token) throws CustomException;

    // Save token to redis with expire time

    Long setDataWithExpiredTime(String token, Long unixTime) throws CustomException;
}
