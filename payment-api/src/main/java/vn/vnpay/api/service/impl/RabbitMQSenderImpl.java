package vn.vnpay.api.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vnpay.api.config.RabbitMQConfig;
import vn.vnpay.api.dto.PaymentDTO;
import vn.vnpay.api.exception.CustomException;
import vn.vnpay.api.service.IRabbitMQSender;
import vn.vnpay.api.util.JsonUtil;

@Service
public class RabbitMQSenderImpl implements IRabbitMQSender<PaymentDTO> {

    private final RabbitTemplate rabbitTemplate;

    private static final Logger LOGGER = LogManager.getLogger(RabbitMQSenderImpl.class);

    public RabbitMQSenderImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public String send(PaymentDTO object) {
        LOGGER.info("Send payment info to rabbitmq: {}", object);
        Object data = rabbitTemplate.convertSendAndReceive(RabbitMQConfig.directExchangeName, RabbitMQConfig.routingPayment, object);
        LOGGER.info("Data receive from consumer: {}", data);
        return String.valueOf(data);
    }
}
