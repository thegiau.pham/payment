package vn.vnpay.api.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import vn.vnpay.api.constant.PaymentDefault;
import vn.vnpay.api.dto.PaymentDTO;
import vn.vnpay.api.enums.MessageType;
import vn.vnpay.api.exception.CustomException;
import vn.vnpay.api.model.ResultEntity;
import vn.vnpay.api.service.BaseService;
import vn.vnpay.api.service.IPaymentService;
import vn.vnpay.api.util.HashUtil;
import vn.vnpay.api.util.JsonUtil;
import vn.vnpay.api.validate.PaymentValidator;
import vn.vnpay.api.validate.Validator;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

@Service
public class PaymentServiceImpl extends BaseService implements IPaymentService {

    private static final Logger LOGGER = LogManager.getLogger(PaymentServiceImpl.class);

    private final PaymentValidator paymentValidator;
    private final RedisServiceImpl redisService;
    private final RabbitMQSenderImpl rabbitMQSender;

    public PaymentServiceImpl(RedisServiceImpl redisService, RabbitMQSenderImpl rabbitMQSender, PaymentValidator paymentValidator) {
        this.redisService = redisService;
        this.rabbitMQSender = rabbitMQSender;
        this.paymentValidator = paymentValidator;
    }

    @Override
    public ResultEntity sendDataToRabbit(PaymentDTO model) throws CustomException {
        LOGGER.info("Payment request body: {}", model);
        this.validate(model);
        if (!this.saveTokenToRedis(model)) {
            throw getCustomException(
                    MessageType.SERVER_ERROR.getCode(),
                    MessageType.SERVER_ERROR.getMessage()
            );
        }
        ResultEntity resultEntity = JsonUtil.getInstance().toEntity(rabbitMQSender.send(model));
        LOGGER.info("Data response: {}", resultEntity);
        return resultEntity;
    }

    private Boolean saveTokenToRedis(PaymentDTO model) throws CustomException {
        Date date = Date.from(LocalDateTime.now()
                .with(LocalTime.MAX).atZone(ZoneId.systemDefault()).toInstant());
        Long timeExpire = date.getTime() / 1000L;
        Long result = redisService.setDataWithExpiredTime(model.getTokenKey(), timeExpire);
        LOGGER.info("Token expire timestamp: {}", timeExpire);
        return result == 1;
    }

    private void validate(PaymentDTO model) throws CustomException {
        paymentValidator.validateToken(model);
        paymentValidator.validateDate(model);
        paymentValidator.validateBankCode(model);
        paymentValidator.validateChecksum(model);
        paymentValidator.validateAmount(model);
    }

    public String generateValidToken(String tokenRequest) throws CustomException {
        String token = "";
        if(Strings.isBlank(tokenRequest)) {
            do {
                token = HashUtil.getInstance().generateToken(25);
            } while (redisService.exist(token));
        }
        else token = tokenRequest;
        return token;
    }
}
