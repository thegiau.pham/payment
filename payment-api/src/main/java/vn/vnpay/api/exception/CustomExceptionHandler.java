package vn.vnpay.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import vn.vnpay.api.enums.MessageType;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        Error error = mappingErrors(fieldErrors);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    private Error mappingErrors(List<FieldError> fieldErrors) {
        Error error = new Error(MessageType.VALIDATION_ERROR.getCode(), MessageType.VALIDATION_ERROR.getMessage());
        List<Field> errors = fieldErrors
                .stream()
                .map(fieldError -> new Field(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());
        error.setData(errors);
        return error;
    }
}
