package vn.vnpay.api.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Error {
    private String code;
    private String message;
    private Object data;

    public Error(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
