package vn.vnpay.core.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Entity(name = "payment")
@Table(name = "tbl_payment")
public class PaymentEntity extends BaseEntity {
    @Column(name = "col_token_key")
    private String tokenKey;

    @Column(name = "col_api_id")
    private String apiID;

    @Column(name = "col_mobile")
    private String mobile;

    @Column(name = "col_bank_code")
    private String bankCode;

    @Column(name = "col_account_no")
    private String accountNo;

    @Column(name = "col_pay_date")
    private String payDate;

    @Column(name = "col_additional_data")
    private String additionalData;

    @Column(name = "col_debit_amount")
    private BigDecimal debitAmount;

    @Column(name = "col_resp_code")
    private String respCode;

    @Column(name = "col_resp_desc")
    private String respDesc;

    @Column(name = "col_trace_transfer")
    private String traceTransfer;

    @Column(name = "col_message_type")
    private Integer messageType;

    @Column(name = "col_check_sum")
    private String checkSum;

    @Column(name = "col_order_code")
    private String orderCode;

    @Column(name = "col_real_amount")
    private BigDecimal realAmount;

    @Column(name = "col_promotion_code")
    private String promotionCode;

    @Column(name = "col_add_value")
    private String addValue;
}
