package vn.vnpay.core.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class ResultEntity {
    private String code;
    private String message;
    private Object data;
}
