package vn.vnpay.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import vn.vnpay.core.dto.PaymentDTO;
import vn.vnpay.core.entity.PaymentEntity;

public class JsonUtil {
    private static JsonUtil instance;

    private ObjectMapper objectMapper;

    private JsonUtil () {
        this.objectMapper = new ObjectMapper();
    }

    public static JsonUtil getInstance() {
        if (instance == null) {
            synchronized (JsonUtil.class) {
                if (null == instance) {
                    instance = new JsonUtil();
                }
            }
        }
        return instance;
    }

    public PaymentDTO toEntity(String json) throws JsonProcessingException {
        return this.objectMapper.readValue(json, PaymentDTO.class);
    }

    public String convertObjectToJSON(Object obj) throws JsonProcessingException {
        return this.objectMapper.writeValueAsString(obj);
    }
}
