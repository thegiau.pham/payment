package vn.vnpay.core.util;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import vn.vnpay.core.dto.PaymentDTO;
import vn.vnpay.core.entity.PaymentEntity;

@Component
public class MapperUtil {
    public PaymentEntity toEntity(PaymentDTO paymentDTO) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper.map(paymentDTO, PaymentEntity.class);
    }
}
