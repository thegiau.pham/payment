package vn.vnpay.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentDTO implements Serializable {
    private String mobile;
    private String additionalData;
    private String respDesc;
    private String traceTransfer;
    private String promotionCode;
    private String bankCode;
    private Integer messageType;
    private String addValue;
    private String tokenKey;
    private String apiID;
    private String accountNo;
    private String payDate;
    private BigDecimal debitAmount;
    private String respCode;
    private String checkSum;
    private String orderCode;
    private BigDecimal realAmount;
}
