package vn.vnpay.core.exception;

import lombok.Data;

@Data
public class CustomException extends Exception{

    public CustomException(Exception ex) {
        super(ex);
    }

    public CustomException(String message) {
        super(new Exception(message));
        this.code = "500";
    }

    public CustomException(String code, String message) {
        super(message);
        this.code = code;
    }

    public CustomException (String code, Exception ex) {
        super(ex);
        this.code = code;
    }

    private String code;
    private Object data;
}
