package vn.vnpay.core.enums;

interface IOption {
    String getCode();
    String getMessage();
}

public enum MessageType implements IOption {
    SUCCESS("00", "Success"),
    SERVER_ERROR("01", "Internal server error"),
    BAD_REQUEST("05", "Bad request")
    ;

    private final String value;
    private final String message;

    MessageType(String value, String message) {
        this.value = value;
        this.message = message;
    }

    @Override
    public String getCode() {
        return value;
    }

    @Override
    public String getMessage() {
        return message;
    }
}