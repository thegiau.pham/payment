package vn.vnpay.core.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.vnpay.core.dto.PaymentDTO;
import vn.vnpay.core.service.BaseService;
import vn.vnpay.core.service.IRequest;

@Service
public class RequestImpl extends BaseService implements IRequest<Object, PaymentDTO> {

    private static final Logger LOGGER = LogManager.getLogger(RequestImpl.class);

    @Override
    public ResponseEntity<Object> post(PaymentDTO object, String url) {
        LOGGER.info("Data payment for api: {}", object);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
            headers.setContentType(MediaType.APPLICATION_JSON);
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<Object> requestBody = new HttpEntity<>(object, headers);
            LOGGER.info("Post to {} with body {}", url, requestBody);
            ResponseEntity<Object> result = restTemplate.postForEntity(url, requestBody, Object.class);
            LOGGER.info("Response from api partner: {}", result);
            return result;
        } catch (Exception ex) {
            LOGGER.error("Error when request to partner api: ", ex);
            return new ResponseEntity<>(error(ex), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
