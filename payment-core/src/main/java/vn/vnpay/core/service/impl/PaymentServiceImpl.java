package vn.vnpay.core.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import vn.vnpay.core.config.RabbitMQConfig;
import vn.vnpay.core.constant.PaymentConstant;
import vn.vnpay.core.dto.PaymentDTO;
import vn.vnpay.core.entity.PaymentEntity;
import vn.vnpay.core.entity.ResultEntity;
import vn.vnpay.core.enums.MessageType;
import vn.vnpay.core.repository.PaymentRepository;
import vn.vnpay.core.service.BaseService;
import vn.vnpay.core.service.IPaymentService;
import vn.vnpay.core.util.JsonUtil;
import vn.vnpay.core.util.MapperUtil;

@Service
public class PaymentServiceImpl extends BaseService implements IPaymentService<PaymentDTO, Message> {

    private static final Logger LOGGER = LogManager.getLogger(PaymentServiceImpl.class);

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private MapperUtil mapperUtil;

    @Autowired
    private RequestImpl request;

    @Override
    @RabbitListener(queues = RabbitMQConfig.queueSendPayment)
    public String receiveDataFromQueue(PaymentDTO model, Message message) throws JsonProcessingException {
        try {
            ThreadContext.put(PaymentConstant.SESSION_TOKEN, model.getTokenKey());
            LOGGER.info("Data receive from rabbitmq: {}", model);

            // save database
            PaymentEntity paymentEntity = mapperUtil.toEntity(model);
            LOGGER.info("Data store in database: {}", paymentEntity);
            paymentRepository.save(paymentEntity);
            LOGGER.info("Store data successfully!");

            // post to partner api
            ResponseEntity<Object> response= request.post(model, PaymentConstant.VNPAY_PARTNER);
            ResultEntity resultEntity = this.mappingResponseFromPartner(response);
            LOGGER.info("Data response to payment-api: {}", resultEntity);

            return JsonUtil.getInstance().convertObjectToJSON(resultEntity);
        }
        catch (Exception ex) {
            LOGGER.info("Error when receiving data from rabbit: ", ex);
            return JsonUtil.getInstance().convertObjectToJSON(error(ex));
        }
        finally {
            ThreadContext.clearAll();
        }
    }

    private ResultEntity mappingResponseFromPartner (ResponseEntity<Object> response) {
        ResultEntity resultEntity = new ResultEntity();
        if(response.getStatusCode() != HttpStatus.OK) {
            resultEntity.setCode(MessageType.BAD_REQUEST.getCode());
            resultEntity.setMessage(MessageType.BAD_REQUEST.getMessage());
            resultEntity.setData(null);
        }
        if(response.getStatusCode() == HttpStatus.OK) {
            resultEntity.setCode(MessageType.SUCCESS.getCode());
            resultEntity.setMessage(MessageType.SUCCESS.getMessage());
            resultEntity.setData(response.getBody());
        }
        return resultEntity;
    }
}
