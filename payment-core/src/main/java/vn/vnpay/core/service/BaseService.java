package vn.vnpay.core.service;

import vn.vnpay.core.entity.ResultEntity;
import vn.vnpay.core.enums.MessageType;
import vn.vnpay.core.exception.CustomException;

public class BaseService {
    protected ResultEntity error(Exception ex) {
        ResultEntity resultEntity = new ResultEntity();
        resultEntity.setMessage(ex.getMessage());
        if (ex instanceof CustomException) {
            CustomException customException = (CustomException) ex;
            resultEntity.setCode(customException.getCode());
            resultEntity.setData(customException.getData());
        }
        else {
            resultEntity.setCode(MessageType.SERVER_ERROR.getCode());
            resultEntity.setMessage(MessageType.SERVER_ERROR.getMessage());
        }
        return resultEntity;
    }
}
