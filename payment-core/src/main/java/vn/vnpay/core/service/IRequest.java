package vn.vnpay.core.service;

import org.springframework.http.ResponseEntity;

public interface IRequest <K, T>{
    ResponseEntity<K> post (T object, String url);
}
