package vn.vnpay.core.service;

public interface IPaymentService<T, K> {
    String receiveDataFromQueue(T object, K object1) throws Exception;
}
